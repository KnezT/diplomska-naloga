package si.uni_lj.fri.knez.timotej.kaca.igralnoPolje;

public enum Smer {
    GOR,
    DOL,
    LEVO,
    DESNO
}
