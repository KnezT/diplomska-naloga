package si.uni_lj.fri.knez.timotej.kaca;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.notifications.VibrationType;
import com.microsoft.band.sensors.GsrSampleRate;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Random;

import si.uni_lj.fri.knez.timotej.kaca.igralnoPolje.IgralnoPolje;
import si.uni_lj.fri.knez.timotej.kaca.igralnoPolje.Smer;
import si.uni_lj.fri.knez.timotej.kaca.services.ZbiranjePodatkov;
import si.uni_lj.fri.knez.timotej.kaca.views.KacaView;

public class MainActivity extends AppCompatActivity {
    IgralnoPolje igralnoPolje;
    Zanka zanka;
    private int tocke;
    private static final String TAG = "MainActivity";
    private String kodaUporabnika="";
    private KacaView kacaView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        kodaUporabnika = getIntent().getStringExtra("kodaUporabnika");
        handler = new Handler();
        setContentView(R.layout.activity_main);
        igralnoPolje = new IgralnoPolje(20, 15, new SoftReference<MainActivity>(this));
        idEksperimenta = System.currentTimeMillis();
        LocalBroadcastManager.getInstance(this).registerReceiver(mKonecReciever,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.KONEC_IGRE"));

        kacaView = (KacaView) findViewById(R.id.kacaView);
        kacaView.setIgralnoPolje(igralnoPolje);
        zanka = new Zanka(igralnoPolje, kacaView);
        zanka.nadaljuj = true;
        zanka.start();
        pripraviZacetnoStanje();
        pripraviPoskusnoFazo();
        zacniMerjenjePodatkov();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        setTocke(0);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");

        idEksperimenta = savedInstanceState.getLong("idEksperimenta");
        kodaUporabnika = savedInstanceState.getString("kodaUporabnika");
        stanje = savedInstanceState.getInt("stanje");
        zaporedje = savedInstanceState.getIntegerArrayList("zaporedje");

        if (stanje>=0) {
            findViewById(R.id.gumb_koncaj_poskus).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");

        outState.putInt("stanje", stanje);
        outState.putLong("idEksperimenta", idEksperimenta);
        outState.putString("kodaUporabnika", kodaUporabnika);
        outState.putIntegerArrayList("zaporedje", zaporedje);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, ZbiranjePodatkov.class));
        zanka.ustavi();
        super.onDestroy();
    }

    private void pripraviPoskusnoFazo() {
        TextView tockeTextView = findViewById(R.id.main_textview_del);
        String novoBesedilo = getResources().getString(R.string.del_poskus);
        tockeTextView.setText(novoBesedilo);
        igralnoPolje.resetiraj();
        findViewById(R.id.gumb_koncaj_poskus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                igralnoPolje.resetiraj();
                zacniNaslednjoFazo();
                findViewById(R.id.gumb_koncaj_poskus).setVisibility(View.GONE);
                kacaView.invalidate();
            }
        });
    }


    private Runnable seIzvedeVsakoSekundo = new Runnable() {
        @Override
        public void run() {
            if (pocitekTimer>0) {
                handler.postDelayed(seIzvedeVsakoSekundo, 1000);
                ((TextView)findViewById(R.id.textview_pocitek)).setText("Počitek " + pocitekTimer + "s");
                pocitekTimer--;
            } else {
                findViewById(R.id.textview_pocitek).setVisibility(View.GONE);
                findViewById(R.id.kacaView).setVisibility(View.VISIBLE);
                igralnoPolje.resetiraj();
                zavibriraj();
                zacniNaslednjoFazo();
            }
        }
    };
    private int pocitekTimer;
    Handler handler;
    private void zacniPocitek() {
        findViewById(R.id.kacaView).setVisibility(View.GONE);
        findViewById(R.id.textview_pocitek).setVisibility(View.VISIBLE);
        pocitekTimer = 60;
        handler.post(seIzvedeVsakoSekundo);
    }

    private BroadcastReceiver mKonecReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            igralnoPolje.koncajIgro();

            // obvestimo zbiralca podatkov, da nismo v fazi testiranja
            Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.PODATKI_FAZE");
            intentIzhod.putExtra("fazaEksperimenta", -1);
            intentIzhod.putExtra("zaporedniDelEksperimenta", -1);
            LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).sendBroadcast(intentIzhod);
            Intent i = new Intent(MainActivity.this, VprasalnikActivity.class);
            startActivityForResult(i, 1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String odgovoriJSON = data.getStringExtra("rezultati");
                naloziVprasalnikNaParse(odgovoriJSON);
                if (stanje + 1 < steviloTestov){
                    zacniPocitek();
                } else {
                    igralnoPolje.resetiraj();
                    zacniNaslednjoFazo();
                }
            }
        }
    }

    private void naloziVprasalnikNaParse(String odgovoriJSON) {
        ParseObject meritve = new ParseObject("vprasalnik");
        meritve.put("odgovori", odgovoriJSON);

        meritve.put("idEksperimenta", idEksperimenta);
        meritve.put("kodaUporabnika", kodaUporabnika);
        meritve.put("zaporedniDelEksperimenta", stanje);
        meritve.put("fazaEksperimenta", zaporedje.get(stanje));
        meritve.put("hitrostKace", delayMedTicki(zaporedje.get(stanje)));
        meritve.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Vprasalnik", "shranjeno");
                } else {
                    Log.d("Vprasalnik", "Napaka pri shranjevanju");
                    e.printStackTrace();
                }
            }
        });
    }

    private float x;
    private float y;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!igralnoPolje.igraPoteka)
                    igralnoPolje.zacniIgro();
                x = event.getX();
                y = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                float razlikaX = this.x - event.getX();
                float razlikaY = this.y - event.getY();
                if (Math.abs(razlikaX) < 40 && Math.abs(razlikaY) < 40) {
                    x = event.getX();
                    y = event.getY();
                    break;
                }
                Smer smer;
                if (Math.abs(razlikaX) > Math.abs(razlikaY)) {
                    if (razlikaX > 0) {
                        smer = Smer.LEVO;
                    } else {
                        smer = Smer.DESNO;
                    }
                } else {
                    if (razlikaY > 0) {
                        smer = Smer.GOR;
                    } else {
                        smer = Smer.DOL;
                    }
                }
                igralnoPolje.obrni(smer);
                x = event.getX();
                y = event.getY();

                posljiDotikBroadcast();
                break;
        }
        return true;
    }

    private void posljiDotikBroadcast() {
        Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.DOTIK");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentIzhod);
    }

    private void zacniMerjenjePodatkov() {
        Intent intent = new Intent(this, ZbiranjePodatkov.class);
        intent.putExtra("zaporedniDelEksperimenta", stanje);
        intent.putExtra("fazaEksperimenta", stanje < 0 ? -1 : zaporedje.get(stanje));
        intent.putExtra("idEksperimenta", idEksperimenta);
        intent.putExtra("kodaUporabnika", kodaUporabnika);
        startService(intent);
    }

    public void setTocke(final int tocke) {
        this.tocke = tocke;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tockeTextView = findViewById(R.id.main_textview_tocke);
                String novoBesedilo = getResources().getString(R.string.tocke, tocke);
                tockeTextView.setText(novoBesedilo);
            }
        });
        Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.TOCKE");
        intentIzhod.putExtra("tocke", tocke);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentIzhod);
    }

    public int getTocke() {
        return tocke;
    }

    // testiranje del
    private static final int steviloTestov = 3;
    private static final int zacetniDelay = 250;
    private static final int korakDelay = 70;

    public static int delayMedTicki(int tezavnost) {
        return zacetniDelay - (korakDelay * tezavnost);
    }

    private void pripraviZacetnoStanje() {
        Random gen = new Random();
        stanje = -1;
        zaporedje = new ArrayList<>(steviloTestov);
        if (gen.nextBoolean()) {
            zaporedje.add(0);
            zaporedje.add(1);
            zaporedje.add(2);
        } else {
            zaporedje.add(2);
            zaporedje.add(1);
            zaporedje.add(0);
        }
//        for (int i = 0; i < steviloTestov; i++) {
//            Integer tezavnost = gen.nextInt(steviloTestov);
//            while (zaporedje.contains(tezavnost)) {
//                tezavnost = gen.nextInt(steviloTestov);
//            }
//            zaporedje.add(tezavnost);
//        }
    }


    private int stanje;
    private ArrayList<Integer> zaporedje;
    private long idEksperimenta;

    private void zacniNaslednjoFazo() {
        stanje++;
        if (stanje < steviloTestov) {
            TextView tockeTextView = findViewById(R.id.main_textview_del);
            String novoBesedilo = getResources().getString(R.string.del, stanje + 1, steviloTestov);
            tockeTextView.setText(novoBesedilo);

            int tezavnost = zaporedje.get(stanje);
            igralnoPolje.koncajIgro();
            zanka.setIntervalMs(delayMedTicki(tezavnost));
            igralnoPolje.resetiraj();

            // poslji broadcast
            Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.PODATKI_FAZE");
            intentIzhod.putExtra("fazaEksperimenta", zaporedje.get(stanje));
            intentIzhod.putExtra("zaporedniDelEksperimenta", stanje);
            intentIzhod.putExtra("idEksperimenta", idEksperimenta);
            intentIzhod.putExtra("kodaUporabnika", kodaUporabnika);
            LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).sendBroadcast(intentIzhod);
        } else {
            zanka.ustavi();
            zacniOsebnostniTest();
        }
    }

    private void zacniOsebnostniTest() {
        Intent intent = new Intent(this, PersonalityTestActivity.class);
        intent.putExtra("idEksperimenta", idEksperimenta);
        intent.putExtra("kodaUporabnika", kodaUporabnika);
        startActivity(intent);
        this.finish();
    }

    private void zavibriraj() {
        Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.ZAVIBRIRAJ");
        LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).sendBroadcast(intentIzhod);
    }
}
