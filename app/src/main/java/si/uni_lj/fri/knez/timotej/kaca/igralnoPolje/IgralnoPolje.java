package si.uni_lj.fri.knez.timotej.kaca.igralnoPolje;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import java.lang.ref.SoftReference;
import java.util.Random;
import java.util.logging.LogRecord;

import si.uni_lj.fri.knez.timotej.kaca.ApplicationClass;
import si.uni_lj.fri.knez.timotej.kaca.MainActivity;
import si.uni_lj.fri.knez.timotej.kaca.services.ZbiranjePodatkov;

public class IgralnoPolje {
    SoftReference<MainActivity> activity;

    private int visina;
    private int sirina;

    public boolean igraPoteka;

    private Kaca kaca;

    public int sadjeX;
    public int sadjeY;

    private Random gen;

    public IgralnoPolje(int visina, int sirina, SoftReference<MainActivity> activity) {
        this.activity = activity;
        gen = new Random();
        this.visina = visina;
        this.sirina = sirina;
        kaca = new Kaca();
        generirajNovoSadje();
        igraPoteka = false;
    }

    public void resetiraj() {
        koncajIgro();
        activity.get().setTocke(0);
        kaca.pripraviZacetnoStanje(sirina / 2, visina / 2, Smer.GOR, 5);
    }

    private void generirajNovoSadje() {
        do {
            sadjeX = gen.nextInt(sirina);
            sadjeY = gen.nextInt(visina);
        } while (kaca.jeNaCelici(sadjeX, sadjeY));
    }

    public void obdelajPotezo() {
        DelecKace dk = kaca.premakniNaprej();
        if (dk.x == sadjeX && dk.y == sadjeY) {
            kaca.pojej();
            if (activity.get() != null)
                activity.get().setTocke(activity.get().getTocke() + 100);
            generirajNovoSadje();
        }
        // ali se je kaca zaletela
        if (dk.x >= sirina || dk.x < 0 || dk.y < 0 || dk.y >= visina || kaca.jeRepNaCelici(dk.x, dk.y)) {
            // konec igre
            igraPoteka = false;
            resetiraj();
            broadcastIzgubilSi();
        }
    }

    public void zacniIgro() {
        igraPoteka = true;
    }

    public void koncajIgro() {
        igraPoteka = false;
    }

    public void obrni(Smer s) {
        if (!staNasprotniSmeri(s, kaca.getSmer()))
            kaca.obrni(s);
    }

    public int getSirina() {
        return sirina;
    }

    public int getVisina() {
        return visina;
    }

    public Kaca getKaca() {
        return kaca;
    }

    public boolean staNasprotniSmeri(Smer prva, Smer druga) {
        if (prva == Smer.GOR)
            return druga == Smer.DOL;
        if (prva == Smer.DOL)
            return druga == Smer.GOR;
        if (prva == Smer.LEVO)
            return druga == Smer.DESNO;
        if (prva == Smer.DESNO)
            return druga == Smer.LEVO;
        return false;
    }

    private void broadcastIzgubilSi() {
        Intent intent = new Intent("si.uni_lj.fri.knez.timotej.kaca.IZGUBIL_SI");
        LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).sendBroadcast(intent);
    }
}
