package si.uni_lj.fri.knez.timotej.kaca;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.HeartRateConsentListener;

import java.lang.ref.WeakReference;

public class ZacetniActivity extends AppCompatActivity {

    private boolean imamDovoljenjeZaSrcniUtrip = false;
    private boolean zapestnicaJePovezana = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zacetni);
        findViewById(R.id.button_zacni_igro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zacniIgro();
            }
        });
        preveriDovoljenja();
        posodobiUI();

        findViewById(R.id.zacni_so_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pridobiKodoPoskusaInZacniOsebnostniTest();
            }
        });
    }

    private void zacniIgro() {
        if (imamDovoljenjeZaSrcniUtrip) {
            pridobiKodoPoskusaInZacni();
        } else {
            prikaziObvestilo("Nimam dovoljenja za merjenje srčnega utripa.");
        }
    }

    private void pridobiKodoPoskusaInZacni() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        final EditText et = new EditText(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(et);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(ZacetniActivity.this, MainActivity.class);
                intent.putExtra("kodaUporabnika", et.getText().toString());
                startActivity(intent);
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void pridobiKodoPoskusaInZacniOsebnostniTest() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        final EditText et = new EditText(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(et);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(ZacetniActivity.this, PersonalityTestActivity.class);
                intent.putExtra("kodaUporabnika", et.getText().toString());
                intent.putExtra("idEksperimenta", System.currentTimeMillis());
                startActivity(intent);
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void posodobiUI() {
        TextView statusPovezava = findViewById(R.id.status_povezava);
        statusPovezava.setText("Povezava z zapestnico: " + (zapestnicaJePovezana ? "OK" : "X"));

        TextView statusDovoljenje = findViewById(R.id.status_dovoljenje);
        statusDovoljenje.setText("Dovoljenje za branje srčnega utripa: " + (imamDovoljenjeZaSrcniUtrip ? "OK" : "X"));
    }

    private void preveriDovoljenja() {
        new AsyncTask<Void, String, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    getConnectedBandClient();
                    if (client.getSensorManager().getCurrentHeartRateConsent() != UserConsent.GRANTED) {
                        return false;
                    } else {
                        return true;
                    }
                } catch (BandException e) {
                    String exceptionMessage = "";
                    switch (e.getErrorType()) {
                        case UNSUPPORTED_SDK_VERSION_ERROR:
                            exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                            break;
                        case SERVICE_ERROR:
                            exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                            break;
                        default:
                            exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                            break;
                    }
                    prikaziObvestilo(exceptionMessage);
                    return false;

                } catch (Exception e) {
                    prikaziObvestilo(e.getMessage());
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean dovaljenje) {
                imamDovoljenjeZaSrcniUtrip = dovaljenje;
                posodobiUI();
            }
        }.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Preveri dovoljenje");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 0) {
            final WeakReference<Activity> reference = new WeakReference<Activity>(this);
            new HeartRateConsentTask().execute(reference);
            preveriDovoljenja();
        }
        return super.onOptionsItemSelected(item);
    }

    private void prikaziObvestilo(String obvestilo) {
        Log.d("Obvestilo", obvestilo);
        System.out.println(obvestilo);
//        Toast.makeText(this, obvestilo,
//                Toast.LENGTH_LONG).show();
    }

    private class HeartRateConsentTask extends AsyncTask<WeakReference<Activity>, Void, Void> {
        @Override
        protected Void doInBackground(WeakReference<Activity>... params) {
            try {
                if (getConnectedBandClient()) {

                    if (params[0].get() != null) {
                        client.getSensorManager().requestHeartRateConsent(params[0].get(), new HeartRateConsentListener() {
                            @Override
                            public void userAccepted(boolean consentGiven) {
                            }
                        });
                    }
                } else {
                    prikaziObvestilo("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                prikaziObvestilo(exceptionMessage);

            } catch (Exception e) {
                prikaziObvestilo(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            preveriDovoljenja();
        }
    }

    public boolean setZapestnicaJePovezana(boolean zapestnicaJePovezana) {
        this.zapestnicaJePovezana = zapestnicaJePovezana;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                posodobiUI();
            }
        });
        return zapestnicaJePovezana;
    }

    private BandClient client = null;
    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                Toast.makeText(getApplicationContext(), "Band isn't paired with your phone.",
                        Toast.LENGTH_LONG).show();
                setZapestnicaJePovezana(false);
                return false;
            }
            client = BandClientManager.getInstance().create(getBaseContext(), devices[0]);
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            setZapestnicaJePovezana(true);
            return true;
        }

        Toast.makeText(getApplicationContext(), "Band is connecting...",
                Toast.LENGTH_LONG).show();
        return setZapestnicaJePovezana(ConnectionState.CONNECTED == client.connect().await());
    }
}
