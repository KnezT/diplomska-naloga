package si.uni_lj.fri.knez.timotej.kaca;
import android.app.Application;
import android.content.Context;

import com.parse.Parse;

public class ApplicationClass extends Application {

    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.back4app_app_id))
                // if defined
                .clientKey(getString(R.string.back4app_client_key))
                .server(getString(R.string.back4app_server_url))
                .build()
        );
    }

    public static Context getAppContext() {
        return mContext;
    }

}