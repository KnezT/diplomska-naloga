package si.uni_lj.fri.knez.timotej.kaca;

import android.os.Message;
import android.util.Log;

import si.uni_lj.fri.knez.timotej.kaca.igralnoPolje.IgralnoPolje;
import si.uni_lj.fri.knez.timotej.kaca.views.KacaView;

public class Zanka extends Thread {
    IgralnoPolje igralnoPolje;
    boolean nadaljuj;
    KacaView kacaView;
    private int intervalMs = 250;
    private final static String TAG = "Zanka";

    public Zanka(IgralnoPolje igralnoPolje, KacaView kacaView) {
        this.igralnoPolje = igralnoPolje;
        nadaljuj = true;
        this.kacaView = kacaView;
    }

    @Override
    public void run() {
        super.run();
        while (nadaljuj) {
            long zacetniCas = System.currentTimeMillis();
            if (igralnoPolje.igraPoteka) {
                igralnoPolje.obdelajPotezo();

                kacaView.posodobiPoslusalec.sendEmptyMessage(1);
            }
            try {
                sleep(Math.max(1, intervalMs - (System.currentTimeMillis() - zacetniCas)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setIntervalMs(int intervalMs) {
        this.intervalMs = intervalMs;
    }

    public void ustavi() {
        nadaljuj = false;
    }
}
