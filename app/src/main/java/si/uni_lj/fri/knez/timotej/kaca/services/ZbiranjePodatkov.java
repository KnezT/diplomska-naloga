package si.uni_lj.fri.knez.timotej.kaca.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.notifications.VibrationType;
import com.microsoft.band.sensors.BandAccelerometerEvent;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.BandGsrEvent;
import com.microsoft.band.sensors.BandGsrEventListener;
import com.microsoft.band.sensors.BandHeartRateEvent;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.BandRRIntervalEvent;
import com.microsoft.band.sensors.BandRRIntervalEventListener;
import com.microsoft.band.sensors.BandSkinTemperatureEvent;
import com.microsoft.band.sensors.BandSkinTemperatureEventListener;
import com.microsoft.band.sensors.GsrSampleRate;
import com.microsoft.band.sensors.SampleRate;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ZbiranjePodatkov extends Service {
    public static final int MAX_TRAJANJE_FAZE_EKSPERIMENTA_MILIS = 180000;
    public static final int MIN_TRAJANJE_FAZE_EKSPERIMENTA_MILIS = 120000;
    private BandClient client = null;
    private boolean nadaljuj = false;

    // podatki
    private int srcniUtrip = -1;
    private String srcniUtripKvaliteta = "";
    private int prevodnostKoze = -1;
    private float temperaturaKoze = -1;
    private double rrInterval = -1;
    private double dotikiNaSekundo = -1;
    private JSONArray pospeski;
    private int tocke = 0;

    private int fazaEksperimenta = -1;
    private int zaporedniDelEksperimenta = -1;
    private long idEksperimenta = -1;
    private String kodaUporabnika = "";
    private long zacetekFazeMillis = System.currentTimeMillis();

    private List<ParseObject> zaPoslatiNaStreznik;

    Handler handler;
    Handler handler2;
    Handler handler3;

    int poslanihMeritev = 0;

    boolean igraZeKoncana = false;
    Runnable enkratNaSekundo = new Runnable() {
        @Override
        public void run() {
            if (nadaljuj)
                handler.postDelayed(enkratNaSekundo, 1000);
            if (fazaEksperimenta >= 0) {
                prikaziObvestilo("shranjevanje podatkov faza: " + fazaEksperimenta + " zaporedni del: " + zaporedniDelEksperimenta);
                dodajPodatkeVCakalnoVrsto();
                //konec faze
                if (!igraZeKoncana && fazaEksperimenta != -1 && System.currentTimeMillis() - zacetekFazeMillis > MAX_TRAJANJE_FAZE_EKSPERIMENTA_MILIS) {
                    igraZeKoncana = true;
                    System.out.println("Konec igre");
                    Intent intent = new Intent("si.uni_lj.fri.knez.timotej.kaca.KONEC_IGRE");
                    LocalBroadcastManager.getInstance(ZbiranjePodatkov.this).sendBroadcast(intent);
                }
            }

        }
    };

    // na 20 sekund
    Runnable enkratNaDesetSekund = new Runnable() {
        @Override
        public void run() {
            if (nadaljuj)
                handler2.postDelayed(enkratNaDesetSekund, 20000);
            naloziNaStreznikShranjenePodatke();
        }
    };


    private int stDotikovOdPrejsnjeMeritve = 0;
    Runnable merjenjeKlikovNaSekundo = new Runnable() {
        @Override
        public void run() {
            if (nadaljuj)
                handler3.postDelayed(merjenjeKlikovNaSekundo, 5000);
            dotikiNaSekundo = stDotikovOdPrejsnjeMeritve / 5d;
            prikaziObvestilo("Dotiki na sekundo: " + dotikiNaSekundo);
            stDotikovOdPrejsnjeMeritve = 0;
        }
    };

    private void naloziNaStreznikShranjenePodatke() {
        System.out.println("Nalozi na streznik");
        if (zaPoslatiNaStreznik.size() > 0) {
            List<ParseObject> poslji = zaPoslatiNaStreznik;
            zaPoslatiNaStreznik = new ArrayList<>();
            poslanihMeritev += poslji.size();
            System.out.println("poslanih meritev: " + poslanihMeritev);
            ParseObject.saveAllInBackground(poslji);
        }
    }

    private void dodajPodatkeVCakalnoVrsto() {
        ParseObject meritve = new ParseObject("meritve");
        meritve.put("idEksperimenta", idEksperimenta);
        meritve.put("kodaUporabnika", kodaUporabnika);
        meritve.put("zaporedniDelEksperimenta", zaporedniDelEksperimenta);
        meritve.put("fazaEksperimenta", fazaEksperimenta);

        meritve.put("srcniUtrip", srcniUtrip);
        meritve.put("srcniUtripKvaliteta", srcniUtripKvaliteta);
        meritve.put("prevodnostKoze", prevodnostKoze);
        meritve.put("temperaturaKoze", temperaturaKoze);
        meritve.put("rrInterval", rrInterval);

        meritve.put("lokalniCas", new Date());
        meritve.put("tocke", tocke);
        meritve.put("dotikiNaSekundo", dotikiNaSekundo);
        JSONArray pospeskiLokalno = pospeski;
        pospeski = new JSONArray();
        meritve.put("pospeski", pospeskiLokalno.toString());


        zaPoslatiNaStreznik.add(meritve);
    }

    private void dodajRRPodatekVCakalnoVrsto(double rrInterval) {
        ParseObject meritveRRInterval = new ParseObject("MeritveRRInterval");
        meritveRRInterval.put("idEksperimenta", idEksperimenta);
        meritveRRInterval.put("kodaUporabnika", kodaUporabnika);
        meritveRRInterval.put("zaporedniDelEksperimenta", zaporedniDelEksperimenta);
        meritveRRInterval.put("fazaEksperimenta", fazaEksperimenta);
        meritveRRInterval.put("rrInterval", rrInterval);
        meritveRRInterval.put("lokalniCas", new Date());

        zaPoslatiNaStreznik.add(meritveRRInterval);
    }

    private BroadcastReceiver mZavibrirajReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (getConnectedBandClient()) {
                    client.getNotificationManager().vibrate(VibrationType.THREE_TONE_HIGH);
                } else {
                    System.out.println("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                System.out.println(exceptionMessage);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    };

    private BroadcastReceiver mIgralecIzgubiReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!igraZeKoncana && fazaEksperimenta != -1 && System.currentTimeMillis() - zacetekFazeMillis > MIN_TRAJANJE_FAZE_EKSPERIMENTA_MILIS) {
                igraZeKoncana = true;
                System.out.println("Konec igre");
                Intent intentIzhod = new Intent("si.uni_lj.fri.knez.timotej.kaca.KONEC_IGRE");
                LocalBroadcastManager.getInstance(ZbiranjePodatkov.this).sendBroadcast(intentIzhod);
            }
        }
    };

    private BroadcastReceiver mNastaviPodatkeFaze = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            fazaEksperimenta = intent.getIntExtra("fazaEksperimenta", fazaEksperimenta);
            idEksperimenta = intent.getLongExtra("idEksperimenta", idEksperimenta);
            kodaUporabnika = intent.getStringExtra("kodaUporabnika");
            zaporedniDelEksperimenta = intent.getIntExtra("zaporedniDelEksperimenta", zaporedniDelEksperimenta);
            igraZeKoncana = false;
            if (fazaEksperimenta != -1) {
                zacetekFazeMillis = System.currentTimeMillis();
            }
        }
    };

    private BroadcastReceiver mDotikReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stDotikovOdPrejsnjeMeritve++;
        }
    };

    private BroadcastReceiver mSpremembaTockReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            tocke = intent.getIntExtra("tocke", tocke);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        pospeski = new JSONArray();
        handler = new Handler();
        handler2 = new Handler();
        handler3 = new Handler();
        zaPoslatiNaStreznik = new ArrayList<>();
        LocalBroadcastManager.getInstance(this).registerReceiver(mZavibrirajReciever,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.ZAVIBRIRAJ"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mIgralecIzgubiReciever,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.IZGUBIL_SI"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mNastaviPodatkeFaze,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.PODATKI_FAZE"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mDotikReciever,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.DOTIK"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mSpremembaTockReciever,
                new IntentFilter("si.uni_lj.fri.knez.timotej.kaca.TOCKE"));

        idEksperimenta = System.currentTimeMillis();
        kodaUporabnika = "";

    }

    private BandHeartRateEventListener mHeartRateEventListener = new BandHeartRateEventListener() {
        @Override
        public void onBandHeartRateChanged(final BandHeartRateEvent event) {
            if (event != null) {
                srcniUtrip = event.getHeartRate();
                srcniUtripKvaliteta = event.getQuality().toString();
                prikaziObvestilo(String.format("Heart Rate = %d beats per minute Quality = %s\n", event.getHeartRate(), event.getQuality()));
            }
        }
    };

    private BandGsrEventListener mGSREventListener = new BandGsrEventListener() {
        @Override
        public void onBandGsrChanged(BandGsrEvent bandGsrEvent) {
            if (bandGsrEvent != null) {
                prevodnostKoze = bandGsrEvent.getResistance();
                prikaziObvestilo(String.format("Galvanic skin response = %d\n", bandGsrEvent.getResistance()));
            }
        }
    };

    private BandSkinTemperatureEventListener mSkinTemperatureEventListener = new BandSkinTemperatureEventListener() {
        @Override
        public void onBandSkinTemperatureChanged(BandSkinTemperatureEvent bandSkinTemperatureEvent) {
            if (bandSkinTemperatureEvent != null) {
                temperaturaKoze = bandSkinTemperatureEvent.getTemperature();
                prikaziObvestilo(String.format("Temperatura kože = %f\n", bandSkinTemperatureEvent.getTemperature()));
            }
        }
    };

    private BandRRIntervalEventListener mRRIntervalEventListener = new BandRRIntervalEventListener() {
        @Override
        public void onBandRRIntervalChanged(BandRRIntervalEvent bandRRIntervalEvent) {
            if (bandRRIntervalEvent != null) {
                rrInterval = bandRRIntervalEvent.getInterval();
                if (fazaEksperimenta >= 0) {
                    dodajRRPodatekVCakalnoVrsto(bandRRIntervalEvent.getInterval());
                }
                prikaziObvestilo(String.format("RR interval = %f\n", rrInterval));
            }
        }
    };

    private BandAccelerometerEventListener mBandAccelerometerEventListener = new BandAccelerometerEventListener() {
        @Override
        public void onBandAccelerometerChanged(BandAccelerometerEvent bandAccelerometerEvent) {
            if (bandAccelerometerEvent != null) {
                if (fazaEksperimenta >= 0) {
                    float xAcc = bandAccelerometerEvent.getAccelerationX();
                    float yAcc = bandAccelerometerEvent.getAccelerationY();
                    float zAcc = bandAccelerometerEvent.getAccelerationZ();
                    JSONObject object = new JSONObject();
                    try {
                        object.put("x", xAcc);
                        object.put("y", yAcc);
                        object.put("z", zAcc);
                        pospeski.put(object);
                    } catch (JSONException e) {
                    }
                }
            }
        }
    };

    public ZbiranjePodatkov() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void prikaziObvestilo(String obvestilo) {
        Log.d("Obvestilo", obvestilo);
        System.out.println(obvestilo);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new SubscriptionTask().execute();
        zacetekFazeMillis = System.currentTimeMillis();
        fazaEksperimenta = intent.getIntExtra("fazaEksperimenta", fazaEksperimenta);
        zaporedniDelEksperimenta = intent.getIntExtra("zaporedniDelEksperimenta", zaporedniDelEksperimenta);
        idEksperimenta = intent.getLongExtra("idEksperimenta", idEksperimenta);
        kodaUporabnika = intent.getStringExtra("kodaUporabnika");
        igraZeKoncana = false;

        if (!nadaljuj) {
            nadaljuj = true;
            handler.post(enkratNaSekundo);
            handler2.post(enkratNaDesetSekund);
            handler3.post(merjenjeKlikovNaSekundo);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        prikaziObvestilo("onDestroy (Zbiranje podatkov)");
        naloziNaStreznikShranjenePodatke();
        nadaljuj = false;
        if (client != null) {
            try {
                if (getConnectedBandClient()) {
                    prikaziObvestilo("Odstranjevanje vseh poslusalcev");
                    client.getSensorManager().unregisterHeartRateEventListener(mHeartRateEventListener);
                    client.getSensorManager().unregisterGsrEventListener(mGSREventListener);
                    client.getSensorManager().unregisterSkinTemperatureEventListener(mSkinTemperatureEventListener);
                    client.getSensorManager().unregisterRRIntervalEventListener(mRRIntervalEventListener);
                    client.getSensorManager().unregisterAccelerometerEventListener(mBandAccelerometerEventListener);
                }
                client.disconnect().await();
            } catch (InterruptedException e) {
                // Do nothing as this is happening during destroy
            } catch (BandException e) {
                // Do nothing as this is happening during destroy
            }
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mIgralecIzgubiReciever);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNastaviPodatkeFaze);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDotikReciever);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSpremembaTockReciever);
        super.onDestroy();
    }

    private class SubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    if (client.getSensorManager().getCurrentHeartRateConsent() == UserConsent.GRANTED) {
                        client.getSensorManager().registerSkinTemperatureEventListener(mSkinTemperatureEventListener);
                        client.getSensorManager().registerGsrEventListener(mGSREventListener, GsrSampleRate.MS200);
                        client.getSensorManager().registerHeartRateEventListener(mHeartRateEventListener);
                        client.getSensorManager().registerRRIntervalEventListener(mRRIntervalEventListener);
                        client.getSensorManager().registerAccelerometerEventListener(mBandAccelerometerEventListener, SampleRate.MS128);
                        prikaziObvestilo("Event listener registriran");
                    } else {
                        prikaziObvestilo("You have not given this application consent to access heart rate data yet."
                                + " Please press the Heart Rate Consent button.\n");
                    }
                } else {
                    prikaziObvestilo("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                prikaziObvestilo(exceptionMessage);

            } catch (Exception e) {
                prikaziObvestilo(e.getMessage());
            }
            return null;
        }
    }


    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                Toast.makeText(getApplicationContext(), "Band isn't paired with your phone.",
                        Toast.LENGTH_LONG).show();
                return false;
            }
            client = BandClientManager.getInstance().create(getBaseContext(), devices[0]);
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            return true;
        }

        Toast.makeText(getApplicationContext(), "Band is connecting...",
                Toast.LENGTH_LONG).show();
        return ConnectionState.CONNECTED == client.connect().await();
    }
}
