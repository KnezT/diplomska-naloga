package si.uni_lj.fri.knez.timotej.kaca;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import si.uni_lj.fri.knez.timotej.kaca.services.ZbiranjePodatkov;

public class VprasalnikActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vprasalnik);
        findViewById(R.id.button_koncaj_vprasalnik).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject rezultati = poberiRezultateVprasalnika();

                if (rezultati!=null) {
                    //broadcast
//                    Intent broadcastIntent = new Intent("si.uni_lj.fri.knez.timotej.kaca.VPRASALNIK");
//                    broadcastIntent.putExtra("rezultati", rezultati.toString());
//                    LocalBroadcastManager.getInstance(VprasalnikActivity.this).sendBroadcast(broadcastIntent);

                    //odgovor
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("rezultati", rezultati.toString());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(VprasalnikActivity.this, "Prislo je do napake", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private JSONObject poberiRezultateVprasalnika() {
        try {
            JSONObject object = new JSONObject();
            SeekBar seekBar_zahtevnost_igre = findViewById(R.id.seekBar_zahtevnost_igre);
            object.put("zahtevnost_igre", seekBar_zahtevnost_igre.getProgress());
            SeekBar seekBar_zabavnost_igre = findViewById(R.id.seekBar_zabavnost_igre);
            object.put("zabavnost_igre", seekBar_zabavnost_igre.getProgress());
            SeekBar seekBar_mental_demand = findViewById(R.id.seekBar_mental_demand);
            object.put("mental_demand", seekBar_mental_demand.getProgress());
            SeekBar seekBar_physical_demand = findViewById(R.id.seekBar_physical_demand);
            object.put("physical_demand", seekBar_physical_demand.getProgress());
            SeekBar seekBar_temporal_demand = findViewById(R.id.seekBar_temporal_demand);
            object.put("temporal_demand", seekBar_temporal_demand.getProgress());
            SeekBar seekBar_performance = findViewById(R.id.seekBar_performance);
            object.put("performance", seekBar_performance.getProgress());
            SeekBar seekBar_effort = findViewById(R.id.seekBar_effort);
            object.put("effort", seekBar_effort.getProgress());
            SeekBar seekBar_frustration = findViewById(R.id.seekBar_frustration);
            object.put("frustration", seekBar_frustration.getProgress());
            return object;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
