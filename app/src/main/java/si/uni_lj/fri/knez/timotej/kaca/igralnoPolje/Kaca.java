package si.uni_lj.fri.knez.timotej.kaca.igralnoPolje;

import java.util.LinkedList;

public class Kaca {
    private int dolzina;
    private LinkedList<DelecKace> deliKace;
    private Smer smer;

    public Kaca() {
        deliKace = new LinkedList<>();
        pripraviZacetnoStanje(2, 2, Smer.DESNO, 2);
    }

    protected void pripraviZacetnoStanje(int x, int y, Smer smer, int dolzina) {
        this.dolzina = dolzina;
        this.smer = smer;
        deliKace.clear();
        for (int i = 0; i < dolzina; i++) {
            switch (smer) {
                case DOL:
                    deliKace.add(new DelecKace(x, y - i));
                    break;
                case GOR:
                    deliKace.add(new DelecKace(x, y + i));
                    break;
                case LEVO:
                    deliKace.add(new DelecKace(x + i, y));
                    break;
                case DESNO:
                    deliKace.add(new DelecKace(x - i, y));
                    break;
            }
        }
    }

    public boolean jeNaCelici(int x, int y) {
        for (DelecKace delec : deliKace) {
            if (delec.x == x && delec.y == y)
                return true;
        }
        return false;
    }

    public boolean jeRepNaCelici(int x, int y) {
        int i = 0;
        for (DelecKace delec : deliKace) {
            if (i > 0 && delec.x == x && delec.y == y)
                return true;
            i++;
        }
        return false;
    }

    public void obrni(Smer s) {
        smer = s;
    }

    public DelecKace naslednjiBlokec() {
        DelecKace noviDelec = izracunajNaslednjiPolozaj();
        return noviDelec;
    }

    public void pojej() {
        dolzina++;
    }

    public DelecKace premakniNaprej() {
        DelecKace noviDelec = izracunajNaslednjiPolozaj();
        deliKace.addFirst(noviDelec);
        while (deliKace.size() > dolzina)
            deliKace.removeLast();
        return noviDelec;
    }

    public boolean jeGlavaNaCelici(int x, int y){
        int xGlave = deliKace.getFirst().x;
        int yGlave = deliKace.getFirst().y;
        return x == xGlave && y == yGlave;
    }

    private DelecKace izracunajNaslednjiPolozaj() {
        int xGlave = deliKace.getFirst().x;
        int yGlave = deliKace.getFirst().y;

        int noviXGlave, noviYGlave;
        switch (smer) {
            case DOL:
                noviXGlave = xGlave;
                noviYGlave = yGlave + 1;
                break;
            case GOR:
                noviXGlave = xGlave;
                noviYGlave = yGlave - 1;
                break;
            case LEVO:
                noviXGlave = xGlave - 1;
                noviYGlave = yGlave;
                break;
            case DESNO:
                noviXGlave = xGlave + 1;
                noviYGlave = yGlave;
                break;
            default:
                noviXGlave = xGlave;
                noviYGlave = yGlave;
        }
        DelecKace noviDelec = new DelecKace(noviXGlave, noviYGlave);
        return noviDelec;
    }

    public Smer getSmer() {
        return smer;
    }
}
