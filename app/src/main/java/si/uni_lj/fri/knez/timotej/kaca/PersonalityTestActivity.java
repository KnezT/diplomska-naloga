package si.uni_lj.fri.knez.timotej.kaca;

import android.content.Intent;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PersonalityTestActivity extends AppCompatActivity {
    private int trenutnoVprasanje = -1;
    private ArrayList<Integer> odgovori;
    private long idEksperimenta;
    private String kodaUporabnika;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        idEksperimenta = intent.getLongExtra("idEksperimenta", -1);
        kodaUporabnika = intent.getStringExtra("kodaUporabnika");
        if (kodaUporabnika == null)
            kodaUporabnika = "test";
        setContentView(R.layout.activity_personality_test);
        setTitle("Test osebnosti");
        odgovori = new ArrayList<>();

        findViewById(R.id.mocno_ne_strinjam).setOnClickListener(new PoslusalecOdgovorov(1));
        findViewById(R.id.ne_strinjam).setOnClickListener(new PoslusalecOdgovorov(2));
        findViewById(R.id.nevtralen).setOnClickListener(new PoslusalecOdgovorov(3));
        findViewById(R.id.strinjam).setOnClickListener(new PoslusalecOdgovorov(4));
        findViewById(R.id.mocno_strinjam).setOnClickListener(new PoslusalecOdgovorov(5));

        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(59);

        naslednjeVprasanje();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        trenutnoVprasanje = savedInstanceState.getInt("trenutnoVprasanje");
        odgovori = savedInstanceState.getIntegerArrayList("odgovori");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("trenutnoVprasanje", trenutnoVprasanje);
        outState.putIntegerArrayList("odgovori", odgovori);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void shraniOdgovor(int odgovor) {
        if (trenutnoVprasanje != odgovori.size()) {
            System.err.println("Nekaj je slo hudo narobe z shranjevanjem odgovorov na vprasanja");
        }
        odgovori.add(odgovor);
    }

    private void naloziOdgovoreNaParse() {
        //TODO
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < vprasanja.length; i++) {
            jsonArray.put(odgovori.get(i));
        }

        ParseObject meritve = new ParseObject("TestOsebnosti");
        meritve.put("jsonOdgovori", jsonArray.toString());
        meritve.put("idEksperimenta", idEksperimenta);
        meritve.put("kodaUporabnika", kodaUporabnika);
        meritve.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Vprasalnik", "shranjeno");
                } else {
                    Log.d("Vprasalnik", "Napaka pri shranjevanju");
                    e.printStackTrace();
                }
            }
        });
        System.out.println(jsonArray.toString());
    }

    private void naslednjeVprasanje() {
        trenutnoVprasanje++;
        if (trenutnoVprasanje >= vprasanja.length) {
            naloziOdgovoreNaParse();
            finish();
        } else {
            TextView vprasanje = findViewById(R.id.vprasanje);
            vprasanje.setText(vprasanja[trenutnoVprasanje]);
            ProgressBar progressBar = findViewById(R.id.progressBar);
            progressBar.setProgress(trenutnoVprasanje);
        }
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Za izhod ponovno pritisni nazaj.", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private static final String[] vprasanja = {
            "Če bi obiskal umetnostno galerijo, bi se precej dolgočasil.",
            "Da bi se izognil hitenju zadnjo minuto, planiram in organiziram stvari vnaprej.",
            "Redko zamerim, tudi ljudem, ki so mi naredili hudo krivico.",
            "Celostno gledano, sem kar zadovoljen sam s sabo.",
            "Strah bi me bilo potovati v slabih vremenskih razmerah.",
            "Tudi če bi mislil, da bo laskanje pomagalo, ga ne bi uporabil za to, da bi v službi napredoval ali dobil povišico.",
            "Zanima me učenje o zgodovini in politiki drugih držav.",
            "Pogosto se močno priganjam, ko skušam doseči nek cilj.",
            "Ljudje včasih pravijo, da sem preveč kritičen do drugih.",
            "Na skupinskih srečanjih redko izrazim svoja mnenja.",
            "Včasih ne morem nehati biti zaskrbljen za malenkosti.",
            "Če bi vedel, da me ne bodo ujeli, bi bil pripravljen ukrasti milijon evrov.",
            "Užival bi v ustvarjanju umetniškega dela, npr. romana, pesmi, slike.",
            "Ko na nečem delam, ne posvečam veliko pozornosti majhnim podrobnostim.",
            "Ljudje včasih pravijo, da sem preveč trmast.",
            "Raje imam dela, ki vključujejo aktivno interakcijo z drugimi ljudmi, kot samostojno delo.",
            "Ko mi je težko ob boleči izkušnji, potrebujem nekoga, da se ob njem bolje počutim.",
            "Ni mi posebej pomembno imeti mnogo denarja.",
            "Mislim, da je posvečanje pozornosti radikalnim idejam potrata časa.",
            "Odločtive sprejemam bolj po občutku, kot po pazljivem razmisleku.",
            "Ljudje menijo, da sem vzkipljiva oseba.",
            "Večino dni sem dobro razpoložen in optimističen.",
            "Na jok mi gre, kadar vidim druge jokati.",
            "Menim, da si zaslužim več spoštovanja kot povprečna oseba.",
            "Če bi imel priložnost, bi šel na koncert klasične glasbe.",
            "Ko delam, imam včasih težave zaradi svoje neorganiziranosti.",
            "Moj odnos do ljudi, ki so z mano slabo ravnali, je \"odpusti in pozabi\".",
            "Čutim, da sem nepriljubljena oseba.",
            "Fizičnih nevarnosti se zelo bojim.",
            "Če hočem nekaj od osebe, se bom smejal njenim najslabšim šalam.",
            "Prebiranje enciklopedije me nikoli ni posebej zabavalo.",
            "Opravim le toliko dela, kolikor je nujno potrebno.",
            "Pri presojanju drugih sem nagnjen k tolerantnosti.",
            "V družabnih situacijah navadno jaz naredim prvo potezo.",
            "Skrbi me mnogo manj kot druge ljudi.",
            "Nikoli ne bi sprejel podkupnine, tudi če bi bila zelo visoka.",
            "Ljudje so mi že velikokrat rekli, da sem poln domišljije.",
            "Pri delu se vedno trudim biti natančen, tudi če zaradi tega porabim več časa.",
            "Ko se ljudje z mano ne strinjajo, sem ponavadi kar fleksibilen glede svojih mnenj.",
            "Ko pridem v novo okolje, vedno najprej poiščem nove prijatelje.",
            "S težkimi situacijami dobro upravljam, ne da bi pri tem potreboval čustveno oporo druge osebe.",
            "Užival bi, če bi posedoval drage, luksuzne dobrine.",
            "Rad imam ljudi, ki imajo neobičajne poglede.",
            "Naredim veliko napak, ker ne premislim, preden nekaj storim.",
            "Večina ljudi se razjezi hitreje, kot jaz.",
            "Večina ljudi je bolj optimističnih in dinamičnih od mene.",
            "Kadar nekdo, ki mi je blizu, odhaja za daljši čas, čutim močna čustva.",
            "Želim, da ljudje vedo, da sem pomembna oseba visokega statusa.",
            "Sebe ne vidim kot umetnika ali ustvarjalno osebo.",
            "Ljudje me pogosto označijo za perfekcionista.",
            "Tudi če ljudje delajo veliko napak, redko rečem kaj negativnega.",
            "Včasih čutim, da sem ničvredna oseba.",
            "Tudi v primeru krizne situacije ne bi čutil panike.",
            "Ne bi se pretvarjal, da nekoga maram, le zato, da bi mi ta oseba delala usluge.",
            "Pogovori o filozofiji me dolgočasijo.",
            "Raje, kot da se držim načrta, delam, kar mi pade na pamet.",
            "Ko mi ljudje povedo, da se motim, je moja prva reakcija pregovarjanje z njimi.",
            "V skupini ljudi sem pogosto jaz tisti, ki govori v imenu skupine.",
            "Tudi v situacijah, kjer večina ljudi postane zelo sentimentalna, ostanem nečustven.",
            "Mikalo bi me, da bi uporabil ponarejen denar, če bi bil prepričan, da me ne bodo ujeli."
    };

    class PoslusalecOdgovorov implements View.OnClickListener {
        private int odgovor;

        public PoslusalecOdgovorov(int odgovor) {
            this.odgovor = odgovor;
        }

        @Override
        public void onClick(View v) {
            shraniOdgovor(odgovor);
            naslednjeVprasanje();
        }
    }
}
