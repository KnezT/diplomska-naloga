package si.uni_lj.fri.knez.timotej.kaca.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

import si.uni_lj.fri.knez.timotej.kaca.igralnoPolje.IgralnoPolje;
import si.uni_lj.fri.knez.timotej.kaca.igralnoPolje.Kaca;

public class KacaView extends View {
    private IgralnoPolje igralnoPolje;
    private Paint barva;
    private int zaobljenost;
    private int rob;

    public Handler posodobiPoslusalec;

    public KacaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        barva = new Paint();
        zaobljenost = 5;
        rob = 3;

        posodobiPoslusalec = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                invalidate();
            }
        };
    }

    public void setIgralnoPolje(IgralnoPolje igralnoPolje) {
        this.igralnoPolje = igralnoPolje;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (igralnoPolje == null)
            return;
        Kaca kaca = igralnoPolje.getKaca();
        barva.setColor(Color.BLUE);
        int visinaEkranaPx = getHeight();
        int sirinaEkranaPx = getWidth();

        double velikostCelice = Math.min((double) visinaEkranaPx / igralnoPolje.getVisina(), (double) sirinaEkranaPx / igralnoPolje.getSirina());

        int leviZamik = (int) ((sirinaEkranaPx - (velikostCelice * igralnoPolje.getSirina())) / 2);
        int zgornjiZamik = (int) ((visinaEkranaPx - (velikostCelice * igralnoPolje.getVisina())) / 2);
        double xEkranPx = leviZamik;
        double yEkranPx = zgornjiZamik;
        for (int yCelica = 0; yCelica < igralnoPolje.getVisina(); yCelica++) {
            for (int xCelica = 0; xCelica < igralnoPolje.getSirina(); xCelica++) {
                if (kaca.jeNaCelici(xCelica, yCelica)) {
                    if (kaca.jeGlavaNaCelici(xCelica, yCelica))
                        barva.setColor(0xFFAA0000);
                    else
                        barva.setColor(Color.RED);

                } else if (igralnoPolje.sadjeX == xCelica && igralnoPolje.sadjeY == yCelica) {
                    barva.setColor(Color.GREEN);
                } else {
                    barva.setColor(Color.LTGRAY);
                }
                canvas.drawRoundRect((int) (xEkranPx + rob), (int) (yEkranPx + rob), (int) (xEkranPx + velikostCelice - rob), (int) (yEkranPx + velikostCelice - rob), zaobljenost, zaobljenost, barva);
                xEkranPx += velikostCelice;
            }
            xEkranPx = leviZamik;
            yEkranPx += velikostCelice;
        }

        if (!igralnoPolje.igraPoteka) {
            barva.setColor(Color.BLACK);
            barva.setTextSize(48f);
            Rect bounds = new Rect();
            String besedilo = "Dotakni se za začetek";
            barva.getTextBounds(besedilo, 0, besedilo.length(), bounds);
            canvas.drawText(besedilo, sirinaEkranaPx/2 - bounds.right/2, visinaEkranaPx/2, barva);
        }
    }
}
